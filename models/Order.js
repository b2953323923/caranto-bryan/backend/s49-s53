const mongoose = require('mongoose')

// Schema
const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true,'User ID is required']
  },
  products: [
    {
      productId: {
        type: String,
        required: [true,'Product Id is required']
      },
      productName:{
        type: String,
        required:[true,'Product Name is required']
      },
      quantity: {
        type: Number,
        required: [true,'Quantity is required']
      }
    }
  ],
  totalAmount: {
    type: Number,
    required: [true,'Total Amount is required']
  },
  purchasedOn: {
    type: Date,
    default: Date.now,
  }
});

module.exports= mongoose.model('Order', orderSchema);
