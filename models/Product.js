const mongoose = require('mongoose')


// Schema
const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, 'Product Name is required']

	},
	description: {
		type: String,
		required:[true, 'Description is required']
	},
	price: {
		type: Number,
		required: [true, 'Product Price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	imgUrl:{
		type: String,
		required:[true,"imgUrl is required"]

	}
})

module.exports = mongoose.model('Product', productSchema);