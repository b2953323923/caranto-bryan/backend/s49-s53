const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User")
const bcrypt = require('bcrypt');
const auth=require('../auth');

module.exports.createOrder = async (req, res) => {
  try {
    const { userId, products } = req.body;

    // Validate if the user exists and is not an admin
    const user = await User.findById(userId);
    if (!user || user.isAdmin) {
      return res.json(false);
    }

    const orderedProducts = [];
    let totalAmount = 0;

    for (const product of products) {
      const foundProduct = await Product.findById(product.productId);
      if (!foundProduct) {
        return res.json(false);
      }

      const orderedProduct = {
        productId: product.productId, 
        productName: product.productName,       
        quantity: product.quantity,
        
      };

      orderedProducts.push(orderedProduct);
      totalAmount += foundProduct.price * product.quantity;
    }

    // Create the order
    const order = new Order({
      userId,
      products: orderedProducts,
      totalAmount,
    });

    const savedOrder = await order.save();
    res.json(true);
  } catch (error) {
    res.json(false);
  }
};



module.exports.getUserOrder = async (req, res) => {
   try {
    const userId = req.user.id; // Assuming the user ID is available in req.user.id

    // Find the user by ID
    const orders = await Order.find({userId});
    if (orders.length === 0) {
      return res.json(false);
    }

    res.json({ orders });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};
module.exports.getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find()

    if(orders.length === 0){
      return res.status(404).json({message: 'No orders found'})
    }

    res.status(200).json({orders});
  }catch(error){
    res.status(500).json({message: 'An error occured while retrieving orders'})
  };
}
