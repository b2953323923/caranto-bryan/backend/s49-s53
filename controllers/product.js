const Product = require("../models/Product");
const User = require("../models/User")


module.exports.createProduct =(req,res)=>{
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		imgUrl: req.body.imgUrl

	})
	return newProduct.save().then((product,error)=>{
		
		if(product){
			return res.send(true);
		
		}else{
			return res.send(false);
		}
	}).catch(error=>res.send(error));
	}

module.exports.getAllProducts = (req,res) => {
		return Product.find({}).then(result=>{
			// console.log(result)
			return res.send(result)
		})
		.catch(err => res.send(err))

}

module.exports.getAllActive = (req,res) =>{
	return Product.find({isActive: true}).then(result =>{
		// console.log(result)
		return res.send(result)
	})
	.catch(err=>res.send(err))
}

module.exports.getSingleProduct = (req,res) =>{
	return Product.findById(req.params.productId).then(result =>{
		// console.log(result)
		return res.send(result)
	})
	.catch(err=>res.send(err))
};


module.exports.updateProduct =(req,res) =>{

	let updatedProduct ={
		name:req.body.name,
		description: req.body.description,
		price: req.body.price,
		imgUrl: req.body.imgUrl
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product,error)=>{
	
		if(error) {
			return res.send(false)
		}
		else{
			
			return res.send(true)
		}
	}) .catch(err => res.send(err))
};

module.exports.archiveProduct =(req,res)=>{
		let archivedProduct={
			isActive:false
		}

	return Product.findByIdAndUpdate(req.params.productId, archivedProduct).then((course,error)=>{

		

		if(error) {
			return res.send(false)
		}
		else{
			
			return res.send(true)
		}
		
	}).catch(err =>res.send(err))

}

module.exports.activateProduct =(req,res)=>{
	let activatedProduct={
		isActive:true
	}

	return Product.findByIdAndUpdate(req.params.productId, activatedProduct).then((product,error)=>{


		if(error) {
			return res.send(false)
		}
		else{
			return res.send(true)
		}
		
	}).catch(err =>res.send(err))

}

module.exports.searchProductByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const product = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(product);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports.searchProductsByPriceRange = async (req, res) => {
     try {
       const { minPrice, maxPrice } = req.body;
  
          // Find courses within the price range
       const products = await Product.find({
        price: { $gte: minPrice, $lte: maxPrice }
      });
      
      res.status(200).json({ products });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for products' });
    }
   };