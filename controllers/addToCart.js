const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User")
const AddToCart = require("../models/AddToCart")
const auth=require('../auth');

const addProductToCart = async (req, res) => {
  
      try {
    // Get the logged-in user's ID from the request object (Assuming it's stored in req.user.id after successful login)
    const userId = req.user.id;

    const { productId, quantity } = req.body;

    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Fetch the product name from the fetched product
    const prodName = product.name; // Assuming the product name field is called 'name' in the Product model

    // Find the user's cart or create a new one if it doesn't exist
    let cart = await AddToCart.findOne({ userId });
    if (!cart) {
      cart = new AddToCart({
        userId,
        products: [{ productId, prodName, quantity }],
        totalAmount: product.price * quantity, // Assuming each product has a "price" field in the Product model
      });
    } else {
      // Check if the product is already in the cart
      const existingProduct = cart.products.find((p) => p.productId === productId);
      if (existingProduct) {
        existingProduct.quantity += quantity;
      } else {
        cart.products.push({ productId, prodName, quantity });
      }
      cart.totalAmount += product.price * quantity;
    }

    await cart.save();

    return res.status(200).json({ message: 'Product added to cart successfully' });
  } catch (err) {
    console.error('Error adding to cart:', err);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = { addProductToCart };




module.exports.updateProductQuantity = async (req, res) => {
 try {
     // Get the logged-in user's ID from the request object (Assuming it's stored in req.user.id after successful login)
    const userId = req.user.id;

    const { productId } = req.params;
    const { quantity } = req.body;

    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Find the user's cart
    const cart = await AddToCart.findOne({ userId });
    if (!cart) {
      return res.status(404).json({ error: 'Cart not found' });
    }

    // Check if the product is in the cart
    const cartProduct = cart.products.find((p) => p.productId === productId);
    if (!cartProduct) {
      return res.status(404).json({ error: 'Product not found in the cart' });
    }

    // Calculate the difference in quantity for the product
    const quantityDifference = quantity - cartProduct.quantity;

    // Update the quantity of the product
    cartProduct.quantity = quantity;

    // Update the total amount in the cart
    cart.totalAmount += quantityDifference * product.price;

    // Save the updated cart
    await cart.save();

    return res.status(200).json({ message: 'Cart quantity updated successfully' });
  } catch (err) {
    console.error('Error updating cart quantity:', err);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports.removeProductFromCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId } = req.body;

    // Find the cart for the user
    let cart = await AddToCart.findOne({ userId });

    if (!cart) {
      return res.json(false);
    }

    // Find the index of the product in the cart
    const productIndex = cart.products.findIndex((product) => product.productId === productId);

    if (productIndex === -1) {
      return res.json(false);
    }

    // Retrieve the quantity and price of the product being removed
    const removedQuantity = cart.products[productIndex].quantity;
    const productPrice = cart.products[productIndex].price || 0; // Handle the case when the price is missing or falsy

    // Update the total amount by subtracting the product's cost (price * quantity) from the current total
    cart.totalAmount -= productPrice * removedQuantity;

    // Remove the product from the cart
    cart.products.splice(productIndex, 1);

    // If there are no products left in the cart, delete the cart from the database
    if (cart.products.length === 0) {
      await AddToCart.deleteOne({ userId });
    } else {
      // Otherwise, save the updated cart
      await cart.save();
    }

    res.json(true);
  } catch (error) {
    res.json(false);
  }
};

module.exports.getCartSubtotal = async (req, res) => {
  try {
    const userId = req.user.id;

    // Find the cart for the user
    let cart = await AddToCart.findOne({ userId });

    if (!cart) {
      return res.status(404).json({ error: 'Cart not found' });
    }

    const getProductDetails = async (productId) => {
      try {
        const product = await Product.findById(productId);

        if (!product) {
          throw new Error('Product not found');
        }

        return {
          productName: product.name,
          productPrice: product.price,
        };
      } catch (error) {
        console.error(error);
        throw new Error('Error fetching product details');
      }
    };

    // Calculate subtotal for each item
    const itemsSubtotal = await Promise.all(
      cart.products.map(async (product) => {
        const { productId, quantity } = product;
        const { productName, productPrice } = await getProductDetails(productId);
        const itemSubtotal = productPrice * quantity;
        return { productId, productName, quantity, itemSubtotal };
      })
    );

    // Calculate total price
    const totalPrice = itemsSubtotal.reduce((acc, item) => acc + item.itemSubtotal, 0);

    res.json({ itemsSubtotal, totalPrice });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};