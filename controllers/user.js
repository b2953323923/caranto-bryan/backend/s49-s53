const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth=require('../auth');


module.exports.checkEmailExists = (req,res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return res.send(true); //"Duplicate email found"
		} else {
			return res.send(false);
		}
	})
};







module.exports.registerUser =(req,res) =>{

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: bcrypt.hashSync(req.body.password, 10)
	})
	return newUser.save().then((user,error)=>{
		// User registration failed
		if(error){
			return res.send(false);
		// User registration successful
		}else{
			return res.send(true);
		}
		// catch() code block will handle other kinds of error.
		// prevents our app from crashing when an error occured in the backend server.
	}).catch(err=>res.send(err));

}

module.exports.loginUser = (req,res)=>{
	return User.findOne({email: req.body.email}).then(result=>{
		// User does not exist.
		console.log(result)
		if(result == null){
			return false
		} else{
			// Created the isPasswordCorrect variable to return the result of comparing the login form password and the database password.
			// compareSync() method is used to compare the non-encrypted password from the login form to the encrypted password from the database.
				// will return either true or false.
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);

			if(isPasswordCorrect){
				// Generate an access token
				// Used the "createAccessToken" method defined in the "auth.js"
				return res.send({access: auth.createAccessToken(result)})
			}
			else{
				return res.send(false);
			}
		}
	}).catch(err => res.send(err));
};

module.exports.getProfile =(req,res)=>{
	return User.findById(req.user.id).then(result=>{
		 
			result.password = ""
			return res.send(result)
		
		
	}).catch(err => res.send(err));
}


module.exports.setUserAsAdmin = async (req, res) => {
  try {
    const { userId } = req.body;

    // Find the user by ID
    const user = await User.findById(userId);
    if (!user) {
      return res.json(false);
    }

    // Update the user as an admin
    user.isAdmin = true;
    await user.save();

    res.json(true);
  } catch (error) {
    res.json(false);
  }
};


module.exports.getUserDetails = async (req, res) => {
  try {
    const { userId } = req.params;

    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    res.json(user);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user;

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    res.json(true);
  } catch (error) {
    res.json(false);
  }
};

