const express = require('express');
const productController = require('../controllers/product');
// [SECTION] Routing Component
const router = express.Router();
const auth = require('../auth');
const {verify, verifyAdmin} = auth;


// Route for creating a product
router.post("/create-product", verify,verifyAdmin, productController.createProduct);


router.get("/getAllProduct",  productController.getAllProducts);

// Route for getting all active products
router.get("/", productController.getAllActive);

// Route for getting a specific product
router.get("/:productId/getSingleProduct", productController.getSingleProduct)

// Route for updating a product
router.put("/:productId/updateProduct",verify,verifyAdmin,productController.updateProduct)

// Route for archiving a product
router.put("/:productId/archiveProduct",verify,verifyAdmin,productController.archiveProduct)

// Route for activating a product
router.put("/:productId/activateProduct",verify,verifyAdmin,productController.activateProduct)

router.post('/search', productController.searchProductByName);

router.post('/searchByPrice', productController.searchProductsByPriceRange);

module.exports = router;