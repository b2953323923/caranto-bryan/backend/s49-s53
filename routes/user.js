const express = require('express');
const userController = require('../controllers/user')
const router = express.Router();
const auth = require('../auth');

// destructure the auth file:
// here we use the verify and verifyAdmin as auth middlewares.
const {verify, verifyAdmin} = auth;


// Route to register user
router.post("/checkEmail", userController.checkEmailExists)
router.post("/register", userController.registerUser)


// Route to login user
router.post("/login", userController.loginUser);
router.get("/details", verify, userController.getProfile)

// Route to set another user as admin - stretch goal
router.put("/setUserAsAdmin",verify,verifyAdmin,userController.setUserAsAdmin)
router.get("/:userId",userController.getUserDetails)
router.put('/reset-password', verify, userController.resetPassword);



module.exports = router;