const express = require('express');
// const userController = require('../controllers/user')
// const productController = require('../controllers/product')
const orderController = require('../controllers/order')
const router = express.Router();
const auth = require('../auth');
const {verify, verifyAdmin} = auth;


router.post("/createOrder", verify, orderController.createOrder)
router.get("/getUserOrder",verify, orderController.getUserOrder)

router.get('/getAllOrders', verify,verifyAdmin, orderController.getAllOrders);


module.exports = router;
