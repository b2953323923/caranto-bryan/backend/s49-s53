const express = require('express');
const addToCartController = require('../controllers/addToCart')
const router = express.Router();
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

router.post("/",verify,addToCartController.addProductToCart)


router.put("/changeQuantity/:productId", verify, addToCartController.updateProductQuantity)


router.delete('/removeProduct/', verify, addToCartController.removeProductFromCart);

router.get('/getCartSubtotal', verify, addToCartController.getCartSubtotal)

module.exports = router;